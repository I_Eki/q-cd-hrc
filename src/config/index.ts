export interface NavMenuItem {
  to?: string;
  icon?: string;
  label?: string;
  items?: NavMenuItem[];
  separator?: boolean;
  inPanel?: boolean;
}

export const navMenuItems: NavMenuItem[] = [
  {
    to: '/',
    icon: 'home',
  },
  {
    label: '产品中心',
    inPanel: true,
    items: [
      {
        label: '合成',
        items: [
          {
            label: '全自动反应釜',
          },
          {
            label: '玻璃反应系统-美国Chemglass',
          },
          {
            label: '玻璃反应釜-国产',
          },
          {
            label: '不锈钢反应釜-国产',
          },
          {
            label: '动态温度控制系统-德国huber',
          },
          {
            label: '动态温度控制系统-国产',
          },
          {
            label: '制冷加热循环器',
          },
          {
            label: '低温制冷循环器、低温冷冻机',
          },
          {
            label: '旋转蒸发仪-进口',
          },
          {
            label: '旋转蒸发仪（国产）',
          },
          {
            label: '循环油浴和水浴',
          },
          {
            label: 'Asynt系列反应模块',
          },
          {
            label: '化学玻璃设备-日本Asahi',
          },
          {
            label: 'Vacuubrand 真空泵',
          },
          {
            label: '均质器-德国Ystral',
          },
          {
            label: '公斤级实验室用耐腐蚀真空设备',
          },
          {
            label: '康宁高通量微通道反应器',
          },
          {
            label: 'VOCS气体冷凝回收装置',
          },
          {
            label: 'TCU温度控制系统',
          },
        ],
      },
      {
        label: '专业设备',
        items: [
          {
            label: '光学接触角仪',
          },
          {
            label: '石英晶体微天平',
          },
          {
            label: 'LB膜分析仪',
          },
          {
            label: 'Diener等离子清洗机',
          },
          {
            label: '中研理工高压反应釜',
          },
          {
            label: 'Kruss表面张力',
          },
          {
            label: '宾德干燥箱',
          },
          {
            label: '卡博莱特马弗炉',
          },
          {
            label: 'Christ 冻干机',
          },
          {
            label: '艾本德离心机',
          },
          {
            label: '海道夫旋转蒸发仪',
          },
          {
            label: 'sigma离心机',
          },
          {
            label: '超低温保存箱',
          },
          {
            label: '诺泽气流粉碎机',
          },
          {
            label: '粘度计',
          },
          {
            label: '水份/超声波测厚仪',
          },
        ],
      },
      {
        label: '实验室常规设备',
        items: [
          {
            label: '灭菌器',
          },
          {
            label: '喷雾干燥器',
          },
          {
            label: '马弗炉',
          },
          {
            label: '恒温箱·干燥箱',
          },
          {
            label: '真空干燥箱',
          },
          {
            label: '恒温培养箱',
          },
          {
            label: '恒温水循环装置',
          },
          {
            label: '恒温水槽',
          },
          {
            label: '震荡器',
          },
          {
            label: '等离子清洗机',
          },
          {
            label: '全自动清洗机（清洗机）',
          },
          {
            label: '德国费舍尔马弗炉',
          },
          {
            label: 'A＆D电子秤/电子天平',
          },
        ],
      },
      {
        label: '实验室家具',
        items: [
          {
            label: '通风柜',
          },
          {
            label: '洁净工作台',
          },
          {
            label: '实验台',
          },
        ],
      },
    ],
  },
  {
    label: '新闻中心',
    items: [
      {
        label: '公司新闻',
      },
      {
        label: '行业动态',
      },
      {
        label: '通知公告',
      },
    ],
  },
  {
    label: '服务支持',
    items: [
      {
        label: '资料下载',
        items: [
          {
            label: '样本下载',
          },
          {
            label: '应用文献下载',
          },
        ],
      },
    ],
  },
  {
    label: '关于',
    items: [
      {
        label: '联系我们',
        items: [
          {
            label: '电子地图',
          },
          {
            label: '留言板',
          },
        ],
      },
      {
        label: '公司简介',
        to: '/intro',
        separator: true,
      },
      {
        label: '公司资质',
      },
      {
        label: '人才招聘',
        separator: true,
      },
    ],
  },
];
