export interface CompanyInfoStateInterface {
  name: string;
  tel: string;
}

function state(): CompanyInfoStateInterface {
  return {
    name: '成都恒睿晨科技有限公司',
    // tel: '13308016114',
    tel: '133****6114',
  };
}

export default state;
