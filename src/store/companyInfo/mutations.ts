import { MutationTree } from 'vuex';
import { CompanyInfoStateInterface } from './state';

const mutation: MutationTree<CompanyInfoStateInterface> = {
  someMutation(/* state: ExampleStateInterface */) {
    // your code
  },
};

export default mutation;
