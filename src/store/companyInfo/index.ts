import { Module } from 'vuex';
import { StateInterface } from '../index';
import state, { CompanyInfoStateInterface } from './state';
import actions from './actions';
import getters from './getters';
import mutations from './mutations';

const companyInfoModule: Module<CompanyInfoStateInterface, StateInterface> = {
  namespaced: true,
  actions,
  getters,
  mutations,
  state,
};

export default companyInfoModule;
