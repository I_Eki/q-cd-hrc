import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import { CompanyInfoStateInterface } from './state';

const actions: ActionTree<CompanyInfoStateInterface, StateInterface> = {
  someAction(/* context */) {
    // your code
  },
};

export default actions;
