import { GetterTree } from 'vuex';
import { StateInterface } from '../index';
import { CompanyInfoStateInterface } from './state';

const getters: GetterTree<CompanyInfoStateInterface, StateInterface> = {
  name: (ctx) => ctx.name,
  tel: (ctx) => ctx.tel,
};

export default getters;
