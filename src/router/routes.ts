import { RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('src/pages/Index/index.vue') },
      // { path: '/about', component: () => import('../pages/about/index.vue') },
      // {
      //   path: '/product/analysis',
      //   component: () => import('../pages/product/analysis.vue'),
      // },
      // {
      //   path: '/product/filter',
      //   component: () => import('../pages/product/filter.vue'),
      // },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue'),
  },
];

export default routes;
