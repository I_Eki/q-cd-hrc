
import { boot } from 'quasar/wrappers';
import { Dialog } from 'quasar';

export default boot(() => {
  Dialog.create({
    title: '提示',
    message: '本页面为开发样例，请勿相信展示的任何信息！概不承担任何法律责任！'
  });
});
